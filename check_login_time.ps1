﻿<#
    .SYNOPSiS
    Writing to DB user state (i.e. login/logout)

    .PARAMETER state
    User state, log in/loguot
#>

#------------------------------------------------------------------------------------
# Writing to DB user state (i.e. login/logout)
#------------------------------------------------------------------------------------

#Requires -Version 2.0
Param
(
[Parameter(Mandatory=$True)]
[string]$State
)

#loading required assembly for access to DB
[void][System.Reflection.Assembly]::LoadWithPartialName("MySql.Data")

#gedding current date
$CurDate = Get-Date -format o

#configuration & credentials for DB
$MySQLAdminUserName = 'dbworker'
$MySQLAdminPassword = 'dbworker'
$MySQLDatabase = 'oncheck'
$MySQLHost = '192.168.52.29'
$ConnectionString = "server=" + $MySQLHost + ";port=3306;uid=" + $MySQLAdminUserName + ";pwd=" + $MySQLAdminPassword + ";database="+$MySQLDatabase

#getting current user & hostname
$user = (Get-WmiObject –Class Win32_ComputerSystem | Select-Object UserName) -replace "UserName=","" -split "\\"
$ures_login = $user[1] -replace "}",""
$hostname = (Get-WmiObject –Class Win32_ComputerSystem | Select-Object DNSHostName) -replace "@{DNSHostName=","" -replace "}",""

#creating connect instance & connecting to db 
$cn = New-Object -TypeName MySql.Data.MySqlClient.MySqlConnection
$cn.ConnectionString = $ConnectionString
$cn.Open()
$cm = New-Object -TypeName MySql.Data.MySqlClient.MySqlCommand

#writing conditions
if ($state -eq "login")
{
	$sql = "INSERT INTO login_out_time (name, time, pc, status) VALUES ( '$ures_login' , '$CurDate', '$hostname', 'loged_in')"
}
elseIf ($state -eq "logout")
{
	$sql = "INSERT INTO login_out_time (name, time, pc, status) VALUES ( '$ures_login' , '$CurDate', '$hostname', 'loged_out')"
}
else
{
	write-host -f red 'State must be "login" or "logout"'
	exit
}

#if conditions is satisfying, writing to DB
$cm.Connection = $cn
$cm.CommandText = $sql
$dr = $cm.ExecuteNonQuery()
