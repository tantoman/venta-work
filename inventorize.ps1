﻿<#
    .SYNOPSiS
    Getting cpu name, machine name, clock speed and socket of cpu on network computers

    .PARAMETER Savepath
    Path to save output file

#>

#------------------------------------------------------------------------------------
# Getting cpu name, machine name, clock speed and socket of cpu on network computers
#------------------------------------------------------------------------------------
#



Param
(
[string]$Savepath = (Split-Path -parent $PSCommandPath)


)


#region
function Get-Cmplist ()
{
	$complist = (net view | Select-String \\) | foreach { $_.ToString().Trim().Substring(2) }
	return $complist
}


function Get-CmpData($CompName)
{
	
	$objComp = New-Object PSObject
	$objComp | Add-Member NoteProperty SystemName -Value $null
	$objComp | Add-Member NoteProperty Name -Value $null
	$objComp | Add-Member NoteProperty ClockSpeed -Value $null
	$objComp | Add-Member NoteProperty SocketDesignation -Value $null
	$objComp | Add-Member NoteProperty Manufacturer -Value $null
	$objComp | Add-Member NoteProperty Product -Value $null
	$objComp | Add-Member NoteProperty Capacity -Value $null
	
	$cpu = gwmi -ComputerName $CompName win32_processor | select SystemName, Name, MaxClockSpeed, SocketDesignation
	$mboard = gwmi -ComputerName $CompName Win32_BaseBoard | select Manufacturer, Product
	$memory = gwmi -ComputerName $CompName win32_physicalmemory | Measure-Object -Sum Capacity | select Sum
	
	$objComp.SystemName = $cpu.SystemName
	$objComp.Name = $cpu.Name
	$objComp.ClockSpeed = $cpu.MaxClockSpeed
	$objComp.SocketDesignation = $cpu.SocketDesignation
	$objComp.Manufacturer = $mboard.Manufacturer
	$objComp.Product = $mboard.Product
	$objComp.Capacity = $memory.Sum/1GB
	
	return $objComp
}

function Write-Out ($CompData)
{
	
	$excel = New-Object -ComObject Excel.Application
	
	$ewb = $excel.Workbooks.Add()
	$ews = $ewb.Worksheets.Item(1)
	
	$ews.name = "Список машин"
	
	$ews.Cells.Item(1, 1) = 'Название ПК'
	$ews.Cells.Item(1, 1).Font.Bold = $true
	$ews.Cells.Item(1, 2) = 'Процессор'
	$ews.Cells.Item(1, 2).Font.Bold = $true
	$ews.Cells.Item(1, 3) = 'Частота'
	$ews.Cells.Item(1, 3).Font.Bold = $true
	$ews.Cells.Item(1, 4) = 'Сокет'
	$ews.Cells.Item(1, 4).Font.Bold = $true
	$ews.Cells.Item(1, 5) = 'Объём памяти (Gb)'
	$ews.Cells.Item(1, 5).Font.Bold = $true
	$ews.Cells.Item(1, 6) = 'Производитель мат.платы'
	$ews.Cells.Item(1, 6).Font.Bold = $true
	$ews.Cells.Item(1, 7) = 'Мат.плата'
	$ews.Cells.Item(1, 7).Font.Bold = $true
	
	
	$row = 2
	foreach ($item in $CompData)
	{
		$ews.Cells.Item($row, 1) = $item.SystemName
		$ews.Cells.Item($row, 2) = $item.Name
		$ews.Cells.Item($row, 3) = $item.ClockSpeed
		$ews.Cells.Item($row, 4) = $item.SocketDesignation
		if ($item.Capacity -le 2)
		{
			$ews.Cells.Item($row, 5) = $item.Capacity
			$ews.Cells.Item($row, 5).Interior.ColorIndex = 3
		}
		Else
		{
			$ews.Cells.Item($row, 5) = $item.Capacity
			$ews.Cells.Item($row, 5).Interior.ColorIndex = 10
		}
		$ews.Cells.Item($row, 6) = $item.Manufacturer
		$ews.Cells.Item($row, 7) = $item.Product
		
		$row++
	}
	
	$usedRange = $ews.UsedRange
	$usedRange.EntireColumn.AutoFit() | Out-Null
	#$excel.Visible = $true
	$ewb.Saveas($Savepath+'\invent.xlsx')

    $excel.Quit()
    [System.Runtime.InteropServices.Marshal]::ReleaseComObject([System.__ComObject]$excel) | Out-Null

}

#endregion


$cmplst = Get-Cmplist
$list = @()
$count = $cmplst.Count
for ($i = 0; $i -lt $cmplst.Count; $i++)
{
	write-progress -activity "collectin data of $count item" -status "$i complite" -percentComplete ($i/$cmplst.Count * 100);
	if (Test-Connection $cmplst[$i] -Count 1 )
	{
		$list += Get-CmpData($cmplst[$i])
	}
}

Write-Out ($list)