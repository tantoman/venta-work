﻿#-------------------------------------------------
# flushdns
#-------------------------------------------------
# Usage: flushing DNS on remoute mashin.
Write-Host "flushing dns"
$comp = (net view | Select-String \\) | foreach {$_.ToString().Trim().Substring(2)}
Invoke-Command -ScriptBlock {ipconfig /flushdns} -ComputerName $comp