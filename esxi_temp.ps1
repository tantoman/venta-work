#user password 7Ujm6Yhn cim_reader port 443
$pass ="7Ujm6Yhn" | ConvertTo-SecureString -AsPlainText -Force
$cred = new-object -typename System.Management.Automation.PSCredential -argumentlist "cim_reader",$pass
$CIMOpt = New-CimSessionOption -SkipCACheck -SkipCNCheck -SkipRevocationCheck -Encoding Utf8 -UseSsl
$session = New-CimSession -Authentication Basic -Credential $cred -ComputerName 192.168.52.3 -Port 443 -SessionOption $CIMOpt
$session | Get-CimInstance CIM_NumericSensor | select caption, CurrentReading
$session | Remove-CimSession